export interface FiltroAdote{
    uSeq: number,
    uf: string,
    cidade: string,
    bairro: string,
    especie: string,
    status: string;
}

