export interface Adotavel{
    seq: number,
    uSeq: number,
    dataNasc: string,
    uf: string,
    cidade: string,
    bairro: string,
    especie: string,
    informacoes: string,
    status: string
}