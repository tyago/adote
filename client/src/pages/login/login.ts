import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ListPage } from '../list/list';
import { UsuarioServiceProvider } from '../../providers/usuario-service/usuario-service';
import { Usuario } from '../../modelos/usuario';
import { AdoteDaoProvider } from '../../providers/adote-dao/adote-dao';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  usuario: Usuario = {
    seq: null,
    login: null,
    senha: null
  };

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private _usuarioService:UsuarioServiceProvider,
    private _adoteDao: AdoteDaoProvider,
    private _alertCtrl: AlertController) {
      this._adoteDao.getUsuarioLogado()
        .then( (usuario) =>{
          if( usuario ){
            console.log("[LoginPage] usuario logado ... deslogando");
            this._adoteDao.deslogarUsuario();
            this.navCtrl.setRoot(ListPage);
          }
          else{
            console.log("[LoginPage] usuario deslogado");
          }
        });
  }

  onSubmit(botao){
    // console.log("[BOTAO]: " + botao);
    if( !this.usuario.login || !this.usuario.senha ){
      this._alertCtrl.create({
        title: 'Preenchimento obrigatório',
        subTitle: 'Preencha todos os campos',
        buttons:[
          {text: 'OK'}
        ]
      }).present();
    }
    else{
      if( botao == "acessar" ){
        this.acessarConta();
      }
      else if( botao == "cadastrar" ){
        this.cadastrarConta();
      }
    }
  }

  acessarConta(){
    this._usuarioService.validar(this.usuario)
      .subscribe(
        (_retUsuario:Usuario)=>{
          // console.log("[subscribe](this.usuario): " + this.usuario.seq + " / " + this.usuario.login + " / " + this.usuario.senha );
          // console.log("[subscribe](_retUsuario): " + _retUsuario.seq + " / " + _retUsuario.login + " / " + _retUsuario.senha );
          // console.log("[subscribe](_retUsuario): " + _retUsuario);
          // console.log("[subscribe](_retUsuario-JSON): " + JSON.stringify(_retUsuario));
          if( _retUsuario != null && _retUsuario.seq != null && _retUsuario.login == this.usuario.login && _retUsuario.senha == this.usuario.senha){
            this.usuario.seq = _retUsuario.seq;
            this._adoteDao.setUsuarioLogado(this.usuario);
            this.navCtrl.setRoot(ListPage);
          }
        },
        (err:any) =>{
          console.log(err.message);
          this._alertCtrl.create({
            title: 'Falha ao validar',
            subTitle: 'Usuário e/ou senha inválidos',
            buttons:[
              {text: 'OK'}
            ]
          }).present();
          }
      );
  }

  cadastrarConta(){
    this._usuarioService.cadastrar(this.usuario)
      .subscribe(
        (_retUsuario:Usuario)=>{
          // console.log("[subscribe](this.usuario): " + this.usuario.seq + " / " + this.usuario.login + " / " + this.usuario.senha );
          // console.log("[subscribe](_retUsuario): " + _retUsuario.seq + " / " + _retUsuario.login + " / " + _retUsuario.senha );
          // console.log("[subscribe](_retUsuario): " + _retUsuario);
          // console.log("[subscribe](_retUsuario-JSON): " + JSON.stringify(_retUsuario));
          if( _retUsuario != null && _retUsuario.seq != null && _retUsuario.login == this.usuario.login && _retUsuario.senha == this.usuario.senha){
            this.usuario.seq = _retUsuario.seq;
            this._alertCtrl.create({
              title: 'Sucesso',
              subTitle: 'Usuário criado',
              buttons:[
                {text: 'OK'}
              ]
            }).present();
            //this.navCtrl.setRoot(ListPage);
          }
        },
        (err:any) =>{
          console.log(err.message);
        }
      );
  }

}
