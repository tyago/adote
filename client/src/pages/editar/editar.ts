import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Adotavel } from '../../modelos/adotavel';
import { AdotavelServiceProvider } from '../../providers/adotavel-service/adotavel-service';
import { AdoteDaoProvider } from '../../providers/adote-dao/adote-dao';
import { DetalhePage } from '../detalhe/detalhe';
import { ListPage } from '../list/list';
import { Usuario } from '../../modelos/usuario';

@IonicPage()
@Component({
  selector: 'page-editar',
  templateUrl: 'editar.html',
})
export class EditarPage {
  usuario:Usuario;
  adotavel: Adotavel;

  opStatus: string[] = ['Adotado','Adotável','Encontrado','Procurado'];
  opUf: string[] = ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO'];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _adotavelService:AdotavelServiceProvider,
    private _adoteDao: AdoteDaoProvider,
    private _alertCtrl: AlertController) 
  {
    this._adoteDao.getUsuarioLogado()
      .then( (usuario:Usuario) =>{
        if( usuario ){
          this.usuario = usuario;
        }
        else{
          this.navCtrl.setRoot(ListPage);
        }
      });
    this.adotavel = this.navParams.get('adotavel');
    if( !this.adotavel ){
      this.adotavel = { seq: null, uSeq: null, dataNasc: null, uf: null, cidade: null, bairro: null, especie: null, informacoes: null, status: null };
    }
    else if(this.adotavel.uSeq != this.usuario.seq){
      this.navCtrl.setRoot(ListPage);      
    }
  }
  
  salvar() {
    this.adotavel.uSeq = this.usuario.seq;
    if( this.adotavel.dataNasc && this.adotavel.uf && this.adotavel.cidade && this.adotavel.bairro && 
      this.adotavel.especie && this.adotavel.informacoes && this.adotavel.status )
    {
      if( this.adotavel.seq == null ){
        this._adotavelService.cadastrar(this.adotavel)
          .subscribe(
            (_retAdotavel:Adotavel)=>{
              if( _retAdotavel.seq && _retAdotavel.uSeq && _retAdotavel.dataNasc && _retAdotavel.uf && _retAdotavel.cidade && 
                _retAdotavel.bairro && _retAdotavel.especie && _retAdotavel.informacoes && _retAdotavel.status )
              {
                this.adotavel = _retAdotavel;
                this.detalhar();
              }
            },
            (err:any) =>{
              console.log(err.message);
              this._alertCtrl.create({
                title: 'Erro',
                subTitle: 'Problema no cadastro',
                buttons:[
                  {text: 'OK'}
                ]
              }).present();
            }
          );
      }
      else{
        //TODO: atualizar adotavel
      }
    }
  }

  detalhar() {
    if( this.adotavel && this.adotavel.seq != null && this.adotavel.uSeq != null ){
      this.navCtrl.setRoot(DetalhePage.name,{adotavel:this.adotavel});
    }
  }
}
