import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AdotavelServiceProvider } from '../../providers/adotavel-service/adotavel-service';
import { AdoteDaoProvider } from '../../providers/adote-dao/adote-dao';
import { Adotavel } from '../../modelos/adotavel';
import { ListPage } from '../list/list';


@IonicPage()
@Component({
  selector: 'page-filtro',
  templateUrl: 'filtro.html',
})
export class FiltroPage {
  adotaveis: Adotavel[];

  opStatus: string[];
  status:string;
  opEspecie: string[];
  especie:string;
  opUf: string[];
  uf:string;
  opCidade: string[];
  cidade:string;
  opBairro: string[];
  bairro:string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _adotavelService:AdotavelServiceProvider,
    private _adoteDao: AdoteDaoProvider,
    private _alertCtrl: AlertController) 
  {
    this.atualizarOpcoes();
  }

  filtrar(){
    if(this.status==""){ this.status = null; }
    if(this.especie==""){ this.especie = null; }
    if(this.uf==""){ this.uf = null; }
    if(this.cidade==""){ this.cidade = null; }
    if(this.bairro==""){ this.bairro = null; }

    this.navCtrl.push(ListPage,
      {titulo: 'Filtrados', filtro: {uSeq:null,uf:this.uf,cidade:this.cidade,bairro:this.bairro,especie:this.especie,status:this.status} }
    );
  }

  atualizarOpcoes(){
    this.opStatus = [];
    this.opEspecie = [];
    this.opUf = [];
    this.opCidade = [];
    this.opBairro = [];

    this._adotavelService.listar()
      .finally( () => {
        this.adotaveis.forEach(adot => {
          if( adot.status != null && this.opStatus.indexOf(adot.status) < 0 ){
            this.opStatus.push(adot.status);
          }
          if( adot.especie != null && this.opEspecie.indexOf(adot.especie) < 0 ){
            this.opEspecie.push(adot.especie);
          }
          if( adot.uf != null && this.opUf.indexOf(adot.uf) < 0 ){
            this.opUf.push(adot.uf);
          }
          if( adot.cidade != null && this.opCidade.indexOf(adot.cidade) < 0 ){
            this.opCidade.push(adot.cidade);
          }
          if( adot.bairro != null && this.opBairro.indexOf(adot.bairro) < 0 ){
            this.opBairro.push(adot.bairro);
          }
        });
        this.opStatus.sort();
        this.opEspecie.sort();
        this.opUf.sort();
        this.opCidade.sort();
        this.opBairro.sort();
        
        this.opStatus.unshift("");
        this.opEspecie.unshift("");
        this.opUf.unshift("");
        this.opCidade.unshift("");
        this.opBairro.unshift("");
      })
      .subscribe(
        (_adotaveis: Adotavel[])=>{
          if( _adotaveis != null ){
            _adotaveis.forEach(element => {
              this._adoteDao.salvarAdotavel(element);
            });
            this.adotaveis = _adotaveis;
          }
        },
        (err:any) =>{
          console.log(err.message);
          this._alertCtrl.create({
            title: 'Falha de conexão',
            subTitle: 'Serão considerados somente dados já existentes',
            buttons:[
              {text: 'OK'}
            ]
          }).present();
          this.adotaveis = this._adoteDao.listarAdotaveis();
        }
      );
  }
}
