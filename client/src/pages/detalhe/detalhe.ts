import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Foto } from '../../modelos/foto';
import { Adotavel } from '../../modelos/adotavel';
import { FotoServiceProvider } from '../../providers/foto-service/foto-service';

/**
 * Generated class for the DetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhe',
  templateUrl: 'detalhe.html',
})
export class DetalhePage {
  url_foto:string = "http://localhost:8080/images/"

  fotos:Foto[];
  adotavel:Adotavel = {seq: null,uSeq: null,dataNasc: null,uf: null,cidade: null,bairro: null,especie: null,informacoes: null,status: null};

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private _alertCtrl: AlertController,    
    private _loadingCtrl: LoadingController,    
    private _fotoService:FotoServiceProvider) {
      this.adotavel = navParams.get('adotavel');
  }
  
  ionViewDidLoad(){
    if( this.adotavel == null ){
      this.navCtrl.pop();
    }
    this.atualizarFotos();
  }

  atualizarFotos(){
    if( !this.adotavel ){
      return;
    }

    let loading = this._loadingCtrl.create({ content: 'Aguarde a busca das fotos' });  
    loading.present();

    this._fotoService.buscarAdotavel(this.adotavel.seq)
      .subscribe(
        (_fotos: Foto[])=>{
          if( _fotos != null ){
            this.fotos = _fotos;
          }
          loading.dismiss();
        },
        (err:any) =>{
          loading.dismiss();
          console.log(err.message);
          this._alertCtrl.create({
            title: 'Falha de conexão',
            subTitle: 'Não serão apresentadas fotos',
            buttons:[
              {text: 'OK'}
            ]
          }).present();
        }
      );
  }
}
