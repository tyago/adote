import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FiltroAdote } from '../../modelos/filtro';
import { Adotavel } from '../../modelos/adotavel';
import { AdoteDaoProvider } from '../../providers/adote-dao/adote-dao';
import { AdotavelServiceProvider } from '../../providers/adotavel-service/adotavel-service';
import { DetalhePage } from '../detalhe/detalhe';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  titulo: string;
  filtro: FiltroAdote;
  adotaveis: Adotavel[];
  
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private _adotavelService:AdotavelServiceProvider,
    private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    private _adoteDao: AdoteDaoProvider) {
      this.titulo = this.navParams.get('titulo');
      if( !this.titulo ){
        this.titulo = "Lista";
      }
      this.filtro = this.navParams.get('filtro');
      if( !this.filtro ){
        this.filtro = {uSeq:null,uf:null,cidade:null,bairro:null,especie:null,status:null};
      }
      //console.log("[ListPage](filtro-JSON): " + JSON.stringify(this.filtro));

      this.atualizarAdotaveis();
  }


  atualizarAdotaveis(){
    let loading = this._loadingCtrl.create({ content: 'Aguarde o carregamento dos dados' });  
    loading.present();

    this._adotavelService.filtrarFiltro(this.filtro)
      .subscribe(
        (_adotaveis: Adotavel[])=>{
          // console.log("[subscribe](this.usuario): " + this.usuario.seq + " / " + this.usuario.login + " / " + this.usuario.senha );
          // console.log("[subscribe](_retUsuario): " + _retUsuario.seq + " / " + _retUsuario.login + " / " + _retUsuario.senha );
          // console.log("[subscribe](_retUsuario): " + _retUsuario);
          // console.log("[atualizarAdotaveis](_adotaveis-JSON): " + JSON.stringify(_adotaveis));
          if( _adotaveis != null ){
            _adotaveis.forEach(element => {
              this._adoteDao.salvarAdotavel(element);
            });
            this.adotaveis = _adotaveis;
          }
          loading.dismiss();
        },
        (err:any) =>{
          loading.dismiss();
          console.log(err.message);
          this._alertCtrl.create({
            title: 'Falha de conexão',
            subTitle: 'Serão apresentados somente dados já existentes',
            buttons:[
              {text: 'OK'}
            ]
          }).present();
          this.adotaveis = this._adoteDao.listarAdotaveis(this.filtro);
        }
      );
  }

  detalhar(event, adot: Adotavel) {
    if( adot ){
      this.navCtrl.push(DetalhePage.name,{adotavel:adot});
    }
  }

}
