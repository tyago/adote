import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Foto } from '../../modelos/foto';

@Injectable()
export class FotoServiceProvider {

  private _url = 'http://localhost:8080/foto';

  constructor(public _http: HttpClient) {
    console.log('Hello FotoServiceProvider Provider');
  }

  listar(){
    return this._http
      .get<Foto[]>(this._url+'/')
      .catch((err) => Observable.of(new Error('Falha ao listar fotos!')));
  }
    
  buscar(_seq:number){
    return this._http
      .get<Foto>(this._url+'/'+_seq)
      .catch((err) => Observable.of(new Error('Falha ao buscar foto '+_seq+'!')));
  }

  buscarAdotavel(_aSeq:number){
    return this._http
      .get<Foto[]>(this._url+'/adotavel/'+_aSeq);
      //.catch((err) => Observable.of(new Error('Falha ao buscar fotos do adotável '+_aSeq+'!')));
  }

  cadastrar(_aSeq:number,_arquivo:Blob){
    let body: FormData = new FormData();
    body.append('aSeq', ""+_aSeq);
    body.append('foto', _arquivo);
     
    return this._http
      .post<Foto>(this._url+'/',body)
      .catch((err) => Observable.of(new Error('Falha ao salvar foto!')));
  }

  deletar(_seq:number){
    return this._http
      .delete(this._url+'/'+_seq)
      .catch((err) => Observable.of(new Error('Falha ao deletar foto!')));
  }

}

