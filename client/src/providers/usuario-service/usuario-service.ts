import { HttpClient } from '@angular/common/http';
// import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from '../../modelos/usuario';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class UsuarioServiceProvider {
  //  private httpOptions = {
  //    headers: new HttpHeaders({
  //     "Content-Type": "application/json; charset=utf-8",
  //     "Authorization": "Basic MGJlOGMxZGEtMDY3Ni00NWY3LWI0ZjYtMjRjMjYzMzhmZmEz"
  //     'Access-Control-Allow-Origin': '*',
  //     'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  //     'Access-Control-Allow-Headers': 'X-Requested-With,content-type'
    //  })
  //  };

  private _url = 'http://localhost:8080/usuario';

  constructor(public _http: HttpClient) {
    console.log('Hello UsuarioServiceProvider Provider');
  }

  cadastrar(_usuario:Usuario){
    return this._http
      .post<Usuario>(this._url+'/',_usuario);//JSON.stringify(_usuario),this.httpOptions)
      //.catch((err) => Observable.of(new Error('Falha ao cadastrar usuário!')));
  }

  validar(_usuario:Usuario){
    // console.log("[validar] - " + JSON.stringify(_usuario) );
    return this._http
      .post<Usuario>(this._url+'/validar/',_usuario);//JSON.stringify(_usuario),this.httpOptions)
      //.catch((err) => Observable.of(new Error('Falha ao validar usuário!')));
  }

  atualizar(_usuario:Usuario){
    return this._http
      .put<Usuario>(this._url+'/',_usuario)
      .catch((err) => Observable.of(new Error('Falha ao atualizar senha de usuário!')));
  }
  
  deletar(_seq:number){
    return this._http
      .delete(this._url+'/'+_seq)
      .catch((err) => Observable.of(new Error('Falha ao deletar usuário!')));
  }
}
