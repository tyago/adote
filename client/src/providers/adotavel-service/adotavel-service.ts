import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Adotavel } from '../../modelos/adotavel';
import { FiltroAdote } from '../../modelos/filtro';

@Injectable()
export class AdotavelServiceProvider {

  private _url = 'http://localhost:8080/adotavel';

  constructor(public _http: HttpClient) {
    console.log('Hello AdotavelServiceProvider Provider');
  }

  listar(){
    return this._http
      .get<Adotavel[]>(this._url+'/');
      //.catch((err) => Observable.of(new Error('Falha ao listar adotáveis!')));
  }
  
  buscar(_seq:number){
    return this._http
      .get<Adotavel>(this._url+'/'+_seq)
      .catch((err) => Observable.of(new Error('Falha ao buscar adotável '+_seq+'!')));
  }

  filtrarFiltro(_filtro:FiltroAdote){
    return this.filtrar(_filtro.uSeq,_filtro.uf,_filtro.cidade,_filtro.bairro,_filtro.especie,_filtro.status);
  }

  filtrar(_uSeq:number,_uf:string,_cidade:string,_bairro:string,_especie:string,_status:string){
    let _adotavel:Adotavel = {
      seq: null,
      uSeq: _uSeq,
      dataNasc: null,
      uf: _uf,
      cidade: _cidade,
      bairro: _bairro,
      especie: _especie,
      informacoes: null,
      status: _status
    };
     
    return this._http
      .post<Adotavel[]>(this._url+'/filtrar',_adotavel);
      // .catch((err) => Observable.of(new Error('Falha ao filtrar adotáveis!')));
  }

  cadastrar(_adotavel:Adotavel){
    return this._http
      .post<Adotavel>(this._url+'/',_adotavel);
      //.catch((err) => Observable.of(new Error('Falha ao cadastrar adotável!')));
  }

  deletar(_seq:number){
    return this._http
      .delete(this._url+'/'+_seq)
      .catch((err) => Observable.of(new Error('Falha ao deletar adotável!')));
  }

}

