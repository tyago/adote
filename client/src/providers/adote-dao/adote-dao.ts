import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage'
import { Usuario } from '../../modelos/usuario';
import { Adotavel } from '../../modelos/adotavel';
import { FiltroAdote } from '../../modelos/filtro';

@Injectable()
export class AdoteDaoProvider {
  private CHAVE_USUARIO_LOGADO = "CHAVE_USUARIO_LOGADO";
  private CHAVE_ADOTAVEL_ = "CHAVE_ADOTAVEL_";

  constructor(private _storage: Storage) {
    console.log('Hello AdoteDaoProvider Provider');
  }

  limparBanco(){
    this._storage.clear();
  }

  deslogarUsuario(){
    this._storage.remove(this.CHAVE_USUARIO_LOGADO);
  }

  getUsuarioLogado():Promise<Usuario>{
    return this._storage.get(this.CHAVE_USUARIO_LOGADO);
  }

  setUsuarioLogado(_usuario:Usuario){
    this._storage.set(this.CHAVE_USUARIO_LOGADO,_usuario);
  }

  salvarAdotavel(_adotavel:Adotavel){
    if( _adotavel ){
      this._storage.set(this.CHAVE_ADOTAVEL_+_adotavel.seq, _adotavel);
    }
  }

  listarAdotaveis(filtro?:FiltroAdote){
    let _adotaveis: Adotavel[] = [];
    this._storage.forEach((_adotavel:Adotavel,_chave:string, _iterationNumber: Number) => {
        if( _chave.startsWith(this.CHAVE_ADOTAVEL_) ){
          if( this.validarFiltroAdote(filtro,_adotavel) ){
            _adotaveis.push(_adotavel);
          }
        }
      });
    return _adotaveis;
  }

  validarFiltroAdote(filtro:FiltroAdote,adotavel:Adotavel): boolean {
    if( adotavel == null ){
        return false;
    }
    if( filtro == null ){
        return true;
    }
    if( (filtro.uSeq == null || filtro.uSeq == adotavel.uSeq) &&
        (filtro.uf == null || filtro.uf == adotavel.uf) &&
        (filtro.cidade == null || filtro.cidade == adotavel.cidade) &&
        (filtro.bairro == null || filtro.bairro == adotavel.bairro) &&
        (filtro.especie == null || filtro.especie == adotavel.especie) &&
        (filtro.status == null || filtro.status == adotavel.status) ){
            return true;
    }
    return false;
  }  
}
