import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { ListPage } from '../pages/list/list';
import { FiltroAdote } from '../modelos/filtro';
import { AdoteDaoProvider } from '../providers/adote-dao/adote-dao';
import { Usuario } from '../modelos/usuario';
import { FiltroPage } from '../pages/filtro/filtro';
import { EditarPage } from '../pages/editar/editar';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //Deslogado
  private MENU_ACESSAR = 
    { titulo: 'Acessar Conta', page: LoginPage, filtro:null };

  //Logado
  private MENU_SAIR = 
    { titulo: "Sair da Conta", page: ListPage, filtro:null };
  private MENU_MEUS_CADASTROS = 
    { titulo: "Meus cadastros", page: ListPage, filtro: {uSeq:null,uf:null,cidade:null,bairro:null,especie:null,status:null} };
  private MENU_NOVO_CADASTRO = 
    { titulo: "Novo cadastro", page: EditarPage, filtro:null };

  //Sempre
  private MENU_FILTRAR = 
    { titulo: 'Filtrar', page: FiltroPage, filtro:null };
  private MENU_TUDO = 
    { titulo: 'Tudo', page: ListPage, filtro:null };
  private MENU_PROCURADOS = 
    { titulo: "Procurados", page: ListPage, filtro: {uSeq:null,uf:null,cidade:null,bairro:null,especie:null,status:'Procurado'} };
  private MENU_ENCONTRADOS = 
    { titulo: "Encontrados", page: ListPage, filtro: {uSeq:null,uf:null,cidade:null,bairro:null,especie:null,status:'Encontrado'} };
  private MENU_ADOTAVEIS = 
    { titulo: "Adotáveis", page: ListPage, filtro: {uSeq:null,uf:null,cidade:null,bairro:null,especie:null,status:'Adotável'} };
  private MENU_ADOTADOS = 
    { titulo: "Adotados", page: ListPage, filtro: {uSeq:null,uf:null,cidade:null,bairro:null,especie:null,status:'Adotado'} };

  pages: Array<{titulo: string, page: any, filtro: FiltroAdote}>;

  rootPage: any = ListPage;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    private _adoteDao: AdoteDaoProvider){
      this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this._adoteDao.limparBanco();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.updateMenu();
    });
  }

  openPage(page:{titulo: string, page: any, filtro: FiltroAdote}) {
    console.log("[openPage]  ... " + JSON.stringify(page));
    switch (page) {
      case this.MENU_SAIR:
        this._adoteDao.deslogarUsuario();
        this.updateMenu();
        this.nav.setRoot(page.page);
        break;
      case this.MENU_MEUS_CADASTROS:
        this.updateMenu();
        this.nav.setRoot(page.page,{titulo: page.titulo, filtro: page.filtro});
        break;
      case this.MENU_NOVO_CADASTRO:
        this.updateMenu();
        this.nav.setRoot(page.page,{titulo: page.titulo });
        break;
      case this.MENU_ACESSAR:
        this.updateMenu();
        this.nav.setRoot(page.page,{titulo: page.titulo });
        break;
      case this.MENU_FILTRAR:
        this.updateMenu();
        this.nav.setRoot(page.page,{titulo: page.titulo});
        break;
      case this.MENU_TUDO:
        this.updateMenu();
        this.nav.setRoot(page.page,{titulo: page.titulo});
        break;
      case this.MENU_PROCURADOS:
      case this.MENU_ENCONTRADOS:
      case this.MENU_ADOTAVEIS:
      case this.MENU_ADOTADOS:
        this.updateMenu();
        this.nav.setRoot(page.page,{titulo: page.titulo, filtro: page.filtro});
        break;
      default:
        this.updateMenu();
        this.nav.setRoot(this.MENU_TUDO.page,{titulo: this.MENU_TUDO.titulo});
        break;
    }
  }

  updateMenu(){
    this.pages = [];
    this._adoteDao.getUsuarioLogado()
      .then( (usuario:Usuario) =>{
        if( usuario ){
          console.log("[MyApp] usuario logado ... " + JSON.stringify(usuario));
          this.pages.push(this.MENU_SAIR);
          this.MENU_MEUS_CADASTROS.filtro.uSeq = usuario.seq;
          this.pages.push(this.MENU_MEUS_CADASTROS);
          this.pages.push(this.MENU_NOVO_CADASTRO);
        }
        else{
          console.log("[MyApp] usuario deslogado" );
          this.pages.push(this.MENU_ACESSAR);
        }
        this.pages.push(this.MENU_FILTRAR);
        this.pages.push(this.MENU_TUDO);
        this.pages.push(this.MENU_PROCURADOS);
        this.pages.push(this.MENU_ENCONTRADOS);
        this.pages.push(this.MENU_ADOTAVEIS);
        this.pages.push(this.MENU_ADOTADOS);
      });
  }

}
