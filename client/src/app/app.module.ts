import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { ListPage } from '../pages/list/list';
import { HttpClientModule } from '@angular/common/http';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UsuarioServiceProvider } from '../providers/usuario-service/usuario-service';
import { AdotavelServiceProvider } from '../providers/adotavel-service/adotavel-service';
import { FotoServiceProvider } from '../providers/foto-service/foto-service';

import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/do';
//import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import { AdoteDaoProvider } from '../providers/adote-dao/adote-dao';
import { IonicStorageModule } from '@ionic/storage';
import { FiltroPage } from '../pages/filtro/filtro';
import { EditarPage } from '../pages/editar/editar';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    ListPage,
    FiltroPage,
    EditarPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name:'adote',
      storeName:'adote',
      driverOrder:['indexeddb']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    ListPage,
    FiltroPage,
    EditarPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioServiceProvider,
    AdotavelServiceProvider,
    FotoServiceProvider,
    AdoteDaoProvider
  ]
})
export class AppModule {}
