var express = require('express');
var router = express.Router();

var usuarioSeqGen = 0;
var usuarios = new Array();

function copyTo(iSrc,iDst){
    if( iSrc >= 0 && iDst >= 0 && iSrc < usuarios.length && iDst < usuarios.length ){
        usuarios[iDst].seq = usuarios[iSrc].seq;
        usuarios[iDst].login = usuarios[iSrc].login;
        usuarios[iDst].senha = usuarios[iSrc].senha;
    }
}

function createUsuario(_login,_senha){
    var _seq = usuarioSeqGen;
    usuarioSeqGen = usuarioSeqGen + 1;    
    var _usuario = {
        seq:_seq,        
        login:_login,
        senha:_senha
    };
    usuarios.push(_usuario);
    return _usuario;
}
function readUsuario(_login){
    for (var i = 0; i < usuarios.length; i++) {
        if( usuarios[i].login == _login ){
            return usuarios[i];
        }
    }
    return null;
}
function updateSenha(_login,_senha){
    for (var i = 0; i < usuarios.length; i++) {
        if( usuarios[i].login == _login ){
            usuarios[i].senha = _senha;
            return usuarios[i];
        }
    }
    return null;
}
function deleteUsuario(_login){
    for (var i = 0; i < usuarios.length; i++){
        if( usuarios[i].login == _login ){
            copyTo(0,i);
            usuarios.shift();
            return true;
        }
    }
    return false;
}


router.post('/', function(req, res){
    if( req.body.login == null )
        res.status(400).send('Login não informado.');
    if( req.body.senha == null )
        res.status(400).send('Senha não informada.');

    var _uRes = readUsuario(req.body.login);
    if( _uRes != null ){
        res.status(400).send('Usuário '+req.body.login+' já existe.');
    }
    else{
        _uRes = createUsuario(req.body.login, req.body.senha);
        if( _uRes != null )
            res.status(200).send(_uRes);
        else
            res.status(500).send('Erro interno.');
    }
});

router.post('/validar', function(req, res){
    var _uRes = readUsuario(req.body.login);
    if( _uRes != null && _uRes.senha == req.body.senha )
        res.status(200).send(_uRes);
    else
        res.sendStatus(401);
});

router.put('/', function(req, res){
    if( req.body.login == null )
        res.status(400).send('Login não informado.');
    if( req.body.senha == null )
        res.status(400).send('Senha não informada.');

    var _uRes = updateSenha(req.body.login,req.body.senha);
    if( _uRes != null )
        res.status(200).send(_uRes);
    else
        res.status(400).send('Usuário '+req.body.login+' inexistente.');
});

router.delete('/:login', function(req, res){
    var _delRes = deleteUsuario(req.params.login);
    if( _delRes )
        res.status(200).send('OK');
    else
        res.status(400).send('Usuário '+req.params.login+' inexistente.');
});

module.exports = router;
