var express = require('express');
var router = express.Router();
//var UFs = ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO'];

var adotSeqGen = 0;
var adots = new Array();

function copyTo(iSrc,iDst){
    if( iSrc >= 0 && iDst >= 0 && iSrc < adots.length && iDst < adots.length ){
        adots[iDst].seq = adots[iSrc].seq;
        adots[iDst].uSeq = adots[iSrc].uSeq;
        adots[iDst].dataNasc = adots[iSrc].dataNasc;
        adots[iDst].uf = adots[iSrc].uf;
        adots[iDst].cidade = adots[iSrc].cidade;
        adots[iDst].bairro = adots[iSrc].bairro;
        adots[iDst].especie = adots[iSrc].especie;
        adots[iDst].informacoes = adots[iSrc].informacoes;
        adots[iDst].status = adots[iSrc].status;
    }
}

function createAdot(_uSeq,_dataNasc,_uf,_cidade,_bairro,_especie,_informacoes,_status){
    var _seq = adotSeqGen;
    adotSeqGen = adotSeqGen + 1;
    var _adot = {
        seq:_seq,
        uSeq:_uSeq,
        dataNasc:_dataNasc,
        uf:_uf,
        cidade:_cidade,
        bairro:_bairro,
        especie:_especie,
        informacoes:_informacoes,
        status:_status
    };
    adots.push(_adot);
    return _adot;
}
function readAdot(_seq){
    for (var i = 0; i < adots.length; i++) {
        if( adots[i].seq == _seq ){
            return adots[i];
        }
    }
    return null;
}
function listarAdotsFiltrados(_uSeq,_uf,_cidade,_bairro,_especie,_status){
    var filtrados = new Array();
    for (var i = 0; i < adots.length; i++){
        if( (_uSeq == null || adots[i].uSeq == _uSeq) 
            && (_uf == null || adots[i].uf == _uf) && (_cidade == null || adots[i].cidade == _cidade) && (_bairro == null || adots[i].bairro == _bairro) 
            && (_especie == null || adots[i].especie == _especie) && (_status == null || adots[i].status == _status) )
        {
            filtrados.push(adots[i]);
        }
    }
    return filtrados;
}
function deleteAdot(_seq){
    for (var i = 0; i < adots.length; i++){
        if( adots[i].seq == _seq ){
            copyTo(0,i);
            adots.shift();
            return true;
        }
    }
    return false;
}


router.get('/', function(req, res){
   res.status(200).send(adots);
});

router.get('/:seq([0-9]+)', function(req, res){
    var _aRes = readAdot(req.params.seq);
    if( _aRes != null )
        res.status(200).send(_aRes);
    else
        res.status(400).send('Adotável '+req.params.seq+' inexistente.');
});

router.post('/filtrar', function(req, res){
    var _fRes = listarAdotsFiltrados(req.body.uSeq, req.body.uf, req.body.cidade, req.body.bairro, req.body.especie, req.body.status);
    if( _fRes != null )
        res.status(200).send(_fRes);
    else
        res.status(500).send('Erro interno.');
});

router.post('/', function(req, res){
    if( req.body.uSeq == null )
        res.status(400).send('Usuário não informado.');
    else if( req.body.dataNasc == null )
        res.status(400).send('Data de nascimento não informada.');
    else if( req.body.uf == null )
        res.status(400).send('UF não informada.');
    else if( req.body.cidade == null )
        res.status(400).send('Cidade não informada.');
    else if( req.body.bairro == null )
        res.status(400).send('Bairro não informado.');
    else if( req.body.especie == null )
        res.status(400).send('Espécie não informada.');
    //else if( req.body.informacoes == null )
    //    res.status(400).send('Sem Informações adicionadas.');
    else if( req.body.status == null )
        res.status(400).send('Status não informado.');
    else {
        var _aRes = createAdot(req.body.uSeq,req.body.dataNasc,req.body.uf,req.body.cidade,req.body.bairro,req.body.especie,req.body.informacoes,req.body.status);
        if( _aRes != null )
            res.status(200).send(_aRes);
        else
            res.status(500).send('Erro interno.');
    }
});

router.delete('/:seq([0-9]+)', function(req, res){
    var _delRes = deleteAdot(req.params.seq);
    if( _delRes )
        res.status(200).send('OK');
    else
        res.status(400).send('Adotável '+req.params.seq+' inexistente.');
});

module.exports = router;
