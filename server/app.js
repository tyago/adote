var express = require('express')
var fileUpload = require('express-fileupload');
var bodyParser = require('body-parser');
var app = express();
var cors = require('cors');

//app.use(fileUpload({
//   limits: { fileSize: 10 * 1024 * 1024 } //10MB
//}));
app.use(fileUpload());
app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

var usuario = require('./usuario.js');
var adotavel = require('./adotavel.js');
var foto = require('./foto.js');
app.use('/usuario', usuario);
app.use('/adotavel', adotavel);
app.use('/foto', foto);

app.use(express.static('public'));

app.get('*', function(req, res){
   res.send('URL inválida.');
});

app.listen(8080, () => { 
   console.log("Servidor ouvindo na porta 8080!"); 
   //console.log(process);
   //console.log(process.env.PWD);
});


