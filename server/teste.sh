#!/bin/bash

echo "Criação dos usuarios"
curl -s -d "login=usuario00&senha=adesempre" -X POST http://localhost:8080/usuario
echo ""
curl -s -d "login=usuario01&senha=adesempre" -X POST http://localhost:8080/usuario
echo ""
curl -s -d "login=usuario02&senha=adesempre" -X POST http://localhost:8080/usuario
echo ""
curl -s -d "login=usuario03&senha=adesempre" -X POST http://localhost:8080/usuario
echo ""

echo "Validando os usuarios"
curl -s -d "login=usuario00&senha=adesempre" -X POST http://localhost:8080/usuario/validar
echo ""
curl -s -d "login=usuario01&senha=adesempre" -X POST http://localhost:8080/usuario/validar
echo ""
curl -s -d "login=usuario02&senha=adesempre" -X POST http://localhost:8080/usuario/validar
echo ""
curl -s -d "login=usuario03&senha=adesempre" -X POST http://localhost:8080/usuario/validar
echo ""

echo "Mudando senhas"
curl -s -d "login=usuario00&senha=mudouessa" -X PUT http://localhost:8080/usuario
echo ""
curl -s -d "login=usuario02&senha=mudouessatambem" -X PUT http://localhost:8080/usuario
echo ""

echo "Deletando usuario"
curl -s -X DELETE http://localhost:8080/usuario/usuario01
echo ""
curl -s -X DELETE http://localhost:8080/usuario/usuario03
echo ""

echo "Validando os usuarios (4F x 2T)"
curl -s -d "login=usuario00&senha=adesempre" -X POST http://localhost:8080/usuario/validar
curl -s -d "login=usuario01&senha=adesempre" -X POST http://localhost:8080/usuario/validar
curl -s -d "login=usuario02&senha=adesempre" -X POST http://localhost:8080/usuario/validar
curl -s -d "login=usuario03&senha=adesempre" -X POST http://localhost:8080/usuario/validar

curl -s -d "login=usuario00&senha=mudouessa" -X POST http://localhost:8080/usuario/validar
echo ""
curl -s -d "login=usuario02&senha=mudouessatambem" -X POST http://localhost:8080/usuario/validar
echo ""
echo ""
#Restaram: 
# {"seq":0,"login":"usuario00","senha":"mudouessa"}
# {"seq":2,"login":"usuario02","senha":"mudouessatambem"}

## ADOTAVEIS
echo "Verificando lista de adotaveis vazia"
curl -X GET http://localhost:8080/adotavel
echo ""

echo "Criação dos adotaveis"
curl -s -d "uSeq=0&dataNasc=04/07/2018&uf=RN&cidade=Natal&bairro=Alecrim&especie=Gato&informacoes=texto qualquer sobre o animal 1 contato: (84)99998-0839&status=Procurado" -X POST http://localhost:8080/adotavel
echo ""
curl -s -d "uSeq=0&dataNasc=03/06/2018&uf=PB&cidade=João Pessoa&bairro=Bessa&especie=Periquito&informacoes=texto qualquer sobre o animal 2 contato: (84)99998-0839&status=Encontrado" -X POST http://localhost:8080/adotavel
echo ""
curl -s -d "uSeq=2&dataNasc=02/05/2018&uf=PE&cidade=Recife&bairro=Recife Antigo&especie=Cachorro&informacoes=texto qualquer sobre o animal 3 contato: (84)99998-0839&status=Adotável" -X POST http://localhost:8080/adotavel
echo ""
curl -s -d "uSeq=2&dataNasc=01/04/2018&uf=AL&cidade=Maceió&bairro=Barro Duro&especie=Papagaio&informacoes=texto qualquer sobre o animal 4 contato: (84)99998-0839&status=Adotado" -X POST http://localhost:8080/adotavel
echo ""

echo "Filtrando adotaveis"
echo "uSeq=0: "
curl -s -d "uSeq=0" -X POST http://localhost:8080/adotavel/filtrar
echo ""
echo "uf=PB: "
curl -s -d "uf=PB" -X POST http://localhost:8080/adotavel/filtrar
echo ""
echo "cidade=Recife: "
curl -s -d "cidade=Recife" -X POST http://localhost:8080/adotavel/filtrar
echo ""
echo "bairro=Barro Duro: "
curl -s -d "bairro=Barro Duro" -X POST http://localhost:8080/adotavel/filtrar
echo ""
echo "especie=Periquito: "
curl -s -d "especie=Periquito" -X POST http://localhost:8080/adotavel/filtrar
echo ""
echo "status=Adotável: "
curl -s -d "status=Adotável" -X POST http://localhost:8080/adotavel/filtrar
echo ""

# echo "Deletando adotaveis"
# curl -s -X DELETE http://localhost:8080/adotavel/0
# echo ""

echo "Verificando lista de adotaveis"
curl -X GET http://localhost:8080/adotavel
echo ""
echo ""

## FOTOS
echo "Enviando fotos"
curl -s -H "Content-Type: multipart/form-data" -F "foto=@/home/tyago/git/adote/server/teste1.jpg" -F "aSeq=1" -X POST http://localhost:8080/foto
echo ""
curl -s -H "Content-Type: multipart/form-data" -F "foto=@/home/tyago/git/adote/server/teste2.gif" -F "aSeq=2" -X POST http://localhost:8080/foto
echo ""
curl -s -H "Content-Type: multipart/form-data" -F "foto=@/home/tyago/git/adote/server/teste1.jpg" -F "aSeq=3" -X POST http://localhost:8080/foto
echo ""
curl -s -H "Content-Type: multipart/form-data" -F "foto=@/home/tyago/git/adote/server/teste2.gif" -F "aSeq=3" -X POST http://localhost:8080/foto
echo ""
