var express = require('express');
var router = express.Router();

const PWD = process.env.PWD;
const SUBDIR_FOTOS = 'public/images';

var fotoSeqGen = 0;
var fotos = new Array();

function copyTo(iSrc,iDst){
    if( iSrc >= 0 && iDst >= 0 && iSrc < fotos.length && iDst < fotos.length ){
        fotos[iDst].seq = fotos[iSrc].seq;
        fotos[iDst].aSeq = fotos[iSrc].aSeq;
        fotos[iDst].data = fotos[iSrc].data;
        fotos[iDst].nome = fotos[iSrc].nome;
    }
}
function createFoto(_aSeq,_nome){
    var _seq = fotoSeqGen;
    fotoSeqGen = fotoSeqGen + 1;
    var _foto = { 
        seq:_seq, 
        aSeq:_aSeq,
        data:new Date(),
        nome:_nome 
    };
    fotos.push(_foto);
    return _foto;
}
function readFoto(_seq){
    for (var i = 0; i < fotos.length; i++) {
        if( fotos[i].seq == _seq ){
            return fotos[i];
        }
    }
    return null;
}
function listarFotosAdotavel(_aSeq){
    var afotos = new Array();
    for (var i = 0; i < fotos.length; i++) {
        if( fotos[i].aSeq == _aSeq ){
            afotos.push(fotos[i]);
        }
    }
    return afotos;
}
function deleteFoto(_seq){
    for (var i = 0; i < fotos.length; i++){
        if( fotos[i].seq == _seq ){
            copyTo(0,i);
            fotos.shift();
            return true;
        }
    }
    return false;
}


router.get('/', function(req, res){
   res.status(200).send(fotos);
});

router.get('/:seq([0-9]+)', function(req, res){
    var _mRes = readFoto(req.params.seq);
    if( _mRes != null )
        res.status(200).send(_mRes);
    else
        res.status(400).send('Foto seq='+req.params.seq+' inexistente.');
});

router.get('/adotavel/:aSeq([0-9]+)', function(req,res){
    var _aRes = listarFotosAdotavel(req.params.aSeq);
    if( _aRes != null )
        res.status(200).send(_aRes);
    else
        res.status(500).send('Erro interno.');
});

router.post('/', function(req, res){
    if(req.body.aSeq == null){
        res.status(400).send('Adotável não informado.');
    }
    else if(req.files == null || req.files.foto == null){
        res.status(400).send('Arquivo \'foto\' não informado.');
    }
    else if( req.files.foto.truncated ){
        res.status(400).send('Falha no envio do arquivo.');
    }
    else if( req.files.foto.mimetype !== "image/jpeg" && req.files.foto.mimetype !== "image/gif" ){
        res.status(400).send('Arquivo informado não é imagem ('+req.files.foto.mimetype+').');
    }
    else{
        let nome = req.files.foto.md5() + "." + req.files.foto.mimetype.substr( "image/".length );
        let destino = PWD + '/' + SUBDIR_FOTOS + '/' + nome;

        req.files.foto.mv(destino, function(err) {
            if (err){
                res.status(500).send(err);
            }
            else{
                var _fRes = createFoto(req.body.aSeq,nome);
                if( _fRes != null ){
                    res.status(200).send(_fRes);
                }
                else{
                    res.status(500).send('Erro interno.');
                }
            }
        });
    }
});

router.delete('/:seq([0-9]+)', function(req, res){
    var _delRes = deleteFoto(req.params.seq);
    if( _delRes )
        res.status(200).send('OK');
    else
        res.status(400).send('Foto '+req.params.seq+' inexistente.');
});

module.exports = router;
